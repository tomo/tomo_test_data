# tomo_test_data

Store data for test related to tomo.

The `main` branch is not really used, except for description.
Each branches provides a bunch of data for some use case.

It uses [LFS](https://docs.gitlab.com/ee/topics/git/lfs/) to allow
to store a huge amount of data without bloating the repository.

The following extension files uses LFS storage: `h5`, `hdf5`, `nx`, `edf`, `tif`, `tiff`

This can be updated with

```
git lfs track "*.dat"
```

